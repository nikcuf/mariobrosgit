package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.mygdx.game.Screens.Play;


public class MyGdxGame extends Game {


    public void create() {
        setScreen(new Play());
    }

     @Override
    public void render() {
        super.render();
    }

    public void dispose() {
       super.dispose();

    }

    @Override public void pause() {
        super.pause();
    }
    @Override public void resume() {
        super.resume();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }
}


