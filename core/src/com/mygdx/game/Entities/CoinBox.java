package com.mygdx.game.Entities;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.mygdx.game.Screens.Play;

import java.util.Iterator;

public class CoinBox implements TiledMapTile {

   private boolean isOpened;
   AnimatedTiledMapTile closedTile;
   TiledMapTile openedTile;
<<<<<<< HEAD
   private Loot loot;
   private int x, y;
=======
   private int x;
   private int y;
   private Loot loot;
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c

   public CoinBox(int x, int y) {
       isOpened = false;
       this.x = x;
       this.y = y;
<<<<<<< HEAD
       loot = new Coin(x, y + 1);
=======
       loot = new Coin(x , y + 1);
       //loot = new Coin();
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
       //Animated tiles

       Iterator<TiledMapTile> tiles = Play.getMap().getTileSets().getTileSet("tileSet").iterator();
       while(tiles.hasNext()) {
           TiledMapTile tile = tiles.next();
           if(tile.getProperties().containsKey("coinBox")) {
               closedTile = (AnimatedTiledMapTile)tile;
           }
           else if(tile.getProperties().containsKey("openedCoinBox")){
               openedTile = tile;
           }
       }

       //End of animated tiles
   }
   //Methods
    public TiledMapTile getCurrentTile(){
       if(isOpened)
           return openedTile;
       else return closedTile;
    }
<<<<<<< HEAD

    public Loot getLoot() {
        return loot;
    }

    public void openCoinBox(){
       loot.makeVisible();
       isOpened = true;
   }

    public void closeCoinBox() { isOpened = false; }
=======
    public void openCoinBox(){
       isOpened = true;
   }

    public boolean isOpened(){
       return isOpened;
    }
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c

    public Loot getLoot(){
    return loot;
    }

    public void closeCoinBox() { isOpened = false; }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public void setId(int i) {
    }

    @Override
    public BlendMode getBlendMode() {
        return null;
    }

    @Override
    public void setBlendMode(BlendMode blendMode) {
    }

    @Override
    public TextureRegion getTextureRegion() {
        return null;
    }

    @Override
    public void setTextureRegion(TextureRegion textureRegion) {

    }

    @Override
    public float getOffsetX() {
        return 0;
    }

    @Override
    public void setOffsetX(float v) {

    }

    @Override
    public float getOffsetY() {
        return 0;
    }

    @Override
    public void setOffsetY(float v) {

    }

    @Override
    public MapProperties getProperties() {
        return null;
    }

}
