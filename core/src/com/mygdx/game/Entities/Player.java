package com.mygdx.game.Entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Screens.Play;

public class Player extends Sprite implements InputProcessor {

    private Vector2 velocity = new Vector2();
    private static TiledMapTileLayer collisionLayer;
    private boolean canJump = true;
    private int lives = 3;

    private float speed = 60 * 4, gravity = 60 * 6;

    public Player(Sprite sprite, TiledMapTileLayer collisionLayer) {
        super(sprite);
        this.collisionLayer = collisionLayer;
    }

    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        super.draw(batch);

    }

    public void update(float delta) {

        //apply gravity
        velocity.y -= gravity * delta;

        //clamp velocity
        MathUtils.clamp(velocity.y, -speed, speed);

        death();

        //save old position
        float oldX = getX(), oldY = getY();

        float tileWidth = 16;
        float tileHeight = 16;
        boolean collisionX = false, collisionY = false, lootCollision = false;

        //move on x
        setX(getX() + velocity.x * delta);

        //Collision detection
        if(velocity.x < 0) { // moving from right to left
            // top left
            if(collisionLayer.getCell((int)(getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight)) != null) {
                collisionX = collisionLayer
                        .getCell((int)(getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight))
                        .getTile()
                        .getProperties()
                        .containsKey("blocked");
                if(collisionX == false)
                    for(CoinBox b: Play.getCoinBoxes()) {
                        collisionX = collisionLayer
<<<<<<< HEAD
                                .getCell((int)(getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight))
                                .getTile()
                                .equals(b.getCurrentTile());
                        if (collisionX == true) break;
=======
                                .getCell((int) (getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight))
                                .getTile()
                                .equals(b.getCurrentTile());
                        if(collisionLayer
                                .getCell((int) (getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight))
                                .getTile()
                                .getProperties()
                                .containsKey("pickup"))
                                b.getLoot().onPickup();

                        if(collisionX == true) break;
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
                    }
                        if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
                                    .getCell((int)(getX() / tileWidth), (int)(getY() + this.getHeight() / tileHeight))
                                    .getTile()
                                    .equals(b.getLoot().getVisibleTile());
                            if(collisionX == true){
                                System.out.println(String.valueOf(collisionX));
                                b.getLoot().onPickup();
                                System.out.println(b.getLoot().getX() + " " + b.getLoot().getY());
                            }
                        }
            }

            //mid left
            if(!collisionX)
                if(collisionLayer.getCell((int)(getX() / tileWidth), (int)((getY() + this.getHeight() /2) / tileHeight)) != null) {
                    collisionX = collisionLayer
<<<<<<< HEAD
                            .getCell((int) (getX() / tileWidth), (int) ((getY() + this.getHeight() / 2) / tileHeight))
=======
                            .getCell((int)(getX() / tileWidth), (int)((getY() + this.getHeight() /2) / tileHeight))
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
<<<<<<< HEAD
                                    .getCell((int) (getX() / tileWidth), (int) ((getY() + this.getHeight() / 2) / tileHeight))
                                    .getTile()
                                    .equals(b.getLoot().getVisibleTile());
                            if(collisionX == true){
                                System.out.println(String.valueOf(collisionX));
                                b.getLoot().onPickup();
                                System.out.println(b.getLoot().getX() + " " + b.getLoot().getY());
                            }
                        }
                }
=======
                                    .getCell((int)(getX() / tileWidth), (int)((getY() + this.getHeight() /2) / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionLayer
                                    .getCell((int)(getX() / tileWidth), (int)((getY() + this.getHeight() /2) / tileHeight))
                                    .getTile()
                                    .getProperties()
                                    .containsKey("pickup"))
                                b.getLoot().onPickup();
                            if(collisionX == true) break;
                        }
                }

>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
            //bottom left
            if(!collisionX)
                if(collisionLayer.getCell((int)(getX() / tileWidth), (int)(getY() / tileHeight)) != null) {
                    collisionX = collisionLayer
<<<<<<< HEAD
                            .getCell((int)(getX() / tileWidth), (int)(getY() / tileHeight))
=======
                            .getCell((int) (getX() / tileWidth), (int) (getY() / tileHeight))
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
<<<<<<< HEAD
                                    .getCell((int)(getX() / tileWidth), (int)(getY() / tileHeight))
                                    .getTile()
                                    .equals(b.getLoot().getVisibleTile());
                            if(collisionX == true){
                                System.out.println(String.valueOf(collisionX));
                                b.getLoot().onPickup();
                                System.out.println(b.getLoot().getX() + " " + b.getLoot().getY());
                            }
                        }
                }
=======
                                    .getCell((int) (getX() / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionLayer
                                    .getCell((int) (getX() / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .getProperties()
                                    .containsKey("pickup"))
                                b.getLoot().onPickup();
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c

                            if(collisionX == true) break;
                        }
                }
        }else if (velocity.x > 0) { //moving from left to right
            // top right
            if(!collisionX)
                if(collisionLayer.getCell((int)((getX() + this.getWidth()) / tileWidth), (int)(getY() + this.getHeight() / tileHeight)) != null) {
                    collisionX = collisionLayer
                            .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() + this.getHeight() / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() + this.getHeight() / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() + this.getHeight() / tileHeight))
                                    .getTile()
                                    .getProperties()
                                    .containsKey("pickup"))
                                b.getLoot().onPickup();

                            if(collisionX == true) break;
                        }

                }
            //mid right
            if(!collisionX)
                if(collisionLayer.getCell((int)((getX() + this.getWidth()) / tileWidth), (int)((getY() + this.getHeight() /2) / tileHeight)) != null) {
                    collisionX = collisionLayer
                            .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) ((getY() + this.getHeight() / 2) / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .getProperties()
                                    .containsKey("pickup"))
                                b.getLoot().onPickup();

                            if(collisionX == true) break;
                        }
                }
            //bottom right
            if(!collisionX)
                if(collisionX = collisionLayer.getCell((int)((getX() + this.getWidth()) / tileWidth), (int)(getY() / tileHeight)) != null) {
                    collisionX = collisionLayer
                            .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionX == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionX = collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionLayer
                                    .getCell((int) ((getX() + this.getWidth()) / tileWidth), (int) (getY() / tileHeight))
                                    .getTile()
                                    .getProperties()
                                    .containsKey("pickup"))
                                b.getLoot().onPickup();

                            if(collisionX == true) break;
                        }

                }
        }
        //react to x collision
        if(collisionX || getX() < -4) {
            setX(oldX);
            velocity.x = 0;
        }

        //move on y
        setY(getY() + velocity.y * delta);

        if(velocity.y < 0) { //falling
            // bottom left
            if(collisionLayer.getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)(getY() / tileHeight)) != null) {
                collisionY = collisionLayer
                        .getCell((int)((getX() + this.getWidth() / 2) / tileWidth), (int)(getY()  / tileHeight))
                        .getTile()
                        .getProperties()
                        .containsKey("blocked");
                if(collisionY == false)
                    for(CoinBox b: Play.getCoinBoxes()){
                        collisionY = collisionLayer
                                .getCell((int)((getX() + this.getWidth() / 2) / tileWidth), (int)(getY()  / tileHeight))
                                .getTile()
                                .equals(b.getCurrentTile());
                        if(collisionY == true) break;
                    }
            }
        //bottom mid
            if(!collisionY)
                if(collisionLayer.getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)((getY() ) / tileHeight)) != null) {
                    collisionY = collisionLayer
                            .getCell((int)((getX() + this.getWidth() /2) / tileWidth), (int)((getY() ) / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionY == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionY = collisionLayer
                                    .getCell((int)((getX() + this.getWidth() /2) / tileWidth), (int)((getY() ) / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionY == true) break;
                        }
                }

            //bottom right
            if(!collisionY)
                if(collisionLayer.getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)(getY() / tileHeight)) != null){
                    collisionY = collisionLayer
                            .getCell((int)((getX() + this.getWidth() / 2 )/ tileWidth), (int)(getY() / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionY == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionY = collisionLayer
                                    .getCell((int)((getX() + this.getWidth() /2) / tileWidth), (int)((getY() ) / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionY == true) break;
                        }
                }
                canJump = collisionY;

        }else if (velocity.y > 0) { // jumping
            // top left
            if(collisionLayer.getCell((int)((getX() + this.getWidth() / 2) / tileWidth), (int)((getY() + this.getHeight()) / tileHeight)) != null){
                collisionY = collisionLayer
                        .getCell((int)((getX() + this.getWidth() / 2) / tileWidth), (int)((getY() + this.getHeight())/ tileHeight))
                        .getTile()
                        .getProperties()
                        .containsKey("blocked");
                if(collisionY == false)
                    for(CoinBox b: Play.getCoinBoxes()){
                        collisionY = collisionLayer
                                .getCell((int)((getX() + this.getWidth() / 2) / tileWidth), (int)((getY() + this.getHeight())/ tileHeight))
                                .getTile()
                                .equals(b.getCurrentTile());
                        if(collisionY == true){
                            b.openCoinBox();
                            break;
                        }
                    }

            }

            //Top mid
            if(!collisionY)
                if(collisionLayer.getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)((getY() + this.getHeight()) / tileHeight)) != null) {
                    collisionY = collisionLayer
                            .getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)((getY() + this.getHeight()) / tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionY == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionY = collisionLayer
                                    .getCell((int)((getX() + this.getWidth() / 2 ) / tileWidth), (int)((getY() + this.getHeight()) / tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionY == true){
                                b.openCoinBox();
                                break;
                            }
                        }
                }

            //Top right
            if(!collisionY)
                if(collisionLayer.getCell((int)(((getX() + this.getWidth() / 2) / tileWidth)), (int)((getY() + this.getHeight()  )/ tileHeight)) != null) {
                    collisionY = collisionLayer
                            .getCell((int)(((getX() + this.getWidth() / 2) / tileWidth)), (int)((getY() + this.getHeight()  )/ tileHeight))
                            .getTile()
                            .getProperties()
                            .containsKey("blocked");
                    if(collisionY == false)
                        for(CoinBox b: Play.getCoinBoxes()){
                            collisionY = collisionLayer
                                    .getCell((int)(((getX() + this.getWidth() / 2) / tileWidth)), (int)((getY() + this.getHeight()  )/ tileHeight))
                                    .getTile()
                                    .equals(b.getCurrentTile());
                            if(collisionY == true){
                                b.openCoinBox();
                                break;
                            }
                        }
                }
             }

        //reacting to collision
        if(collisionY) {
            setY(oldY);
            velocity.y = 0;
        }
        //end of collision detection
    }

    public static void addPoints(int points){
        points += points;
    }

    public void death() {
        if(getY() < 0) {
            lives--;
            if(lives > 0)
                setPosition(4 * getCollisionLayer().getTileWidth(), 4 * getCollisionLayer().getTileHeight());
        }
    }

    //getters and setters
    public float getGravity() {
        return gravity;
    }

    public float getSpeed() {
        return speed;
    }

    public int getLives() {
        return lives;
    }

    public static TiledMapTileLayer getCollisionLayer() {
        return collisionLayer;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setCollisionLayer(TiledMapTileLayer collisionLayer) {
        this.collisionLayer = collisionLayer;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    @Override
    public boolean keyDown(int keycode) { // when a key is pressed
        switch(keycode) {
            case Input.Keys.D:
                velocity.x = speed; break;
            case Input.Keys.A:
                velocity.x = - speed; break;
            case Input.Keys.W:
                if(canJump) {
                    velocity.y = speed;
                    canJump = false;
                }
                break;
        }
        return true;
    }

    public void reset() {
        this.lives = 3;
    }

    @Override
    public boolean keyUp(int keycode) { // when a key is released
        switch (keycode) {
            case Input.Keys.D:
            case Input.Keys.A:
                velocity.x = 0; break;
        }
        return  true;
    }

    @Override
    public boolean keyTyped(char c) {
        return false;
    }

    @Override
    public boolean touchDown(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchUp(int i, int i1, int i2, int i3) {
        return false;
    }

    @Override
    public boolean touchDragged(int i, int i1, int i2) {
        return false;
    }

    @Override
    public boolean mouseMoved(int i, int i1) {
        return false;
    }

    @Override
    public boolean scrolled(int i) {
        return false;
    }
}
