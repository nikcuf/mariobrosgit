package com.mygdx.game.Entities;


import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;

public abstract class Loot {

    public abstract void onPickup();
    public abstract void makeVisible();
    public abstract void makeInvisible();
    public abstract AnimatedTiledMapTile getVisibleTile();
    public abstract AnimatedTiledMapTile getInvisibleTile();
    public abstract AnimatedTiledMapTile getCurrentTile();
    public abstract int getX();
    public abstract int getY();

import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;

public abstract class Loot implements TiledMapTile {

    protected boolean pickedUp;
    protected int x;
    protected int y;
    public abstract AnimatedTiledMapTile getTile();
    public abstract void onPickup();

    public int getY() {
        return y;
    }
    public int getX() {
        return x;
    }

    public boolean isPickedUp(){
        return pickedUp;
    }

}
