package com.mygdx.game.Entities;

<<<<<<< HEAD
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
=======
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.mygdx.game.Screens.Play;

import java.util.Iterator;

public class Coin extends Loot {

<<<<<<< HEAD
    private int x;
    private int y;
    private TiledMapTile currentTile;
    private AnimatedTiledMapTile visibleTile;
    private AnimatedTiledMapTile invisibleTile;
    private TiledMapTileLayer.Cell cell;

    @Override
    public void onPickup() {
        this.makeInvisible();
        System.out.println("picked up coin");
    }

    @Override
    public void makeVisible() {
        currentTile = visibleTile;
    }

    @Override
    public void makeInvisible() {
        currentTile = invisibleTile;
=======
    AnimatedTiledMapTile coinTile;
    AnimatedTiledMapTile emptyTile;

    public void onPickup(){
        Play.addPoints(1);
        System.out.println(Play.getPoints());
        coinTile = emptyTile;
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
    }

    public Coin(int x, int y) {
        this.x = x;
        this.y = y;
<<<<<<< HEAD
        cell = new TiledMapTileLayer.Cell();
        //Getting tiles
        Iterator<TiledMapTile> tiles = Play.getMap().getTileSets().getTileSet("tileSet").iterator();
        while(tiles.hasNext()) {
            TiledMapTile tile = tiles.next();
            if(tile.getProperties().containsKey("coin")) {
                visibleTile = (AnimatedTiledMapTile) tile;
            }
            else if(tile.getProperties().containsKey("invisibleTile")){
                invisibleTile = (AnimatedTiledMapTile) tile;
                currentTile = invisibleTile;
            }
        }
        cell.setTile(currentTile);
        Player.getCollisionLayer().setCell(x, y, cell);
    }

    @Override
    public AnimatedTiledMapTile getCurrentTile() {
        return (AnimatedTiledMapTile) currentTile;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    public TiledMapTileLayer.Cell getCell() {
        return cell;
    }

    public AnimatedTiledMapTile getInvisibleTile() {
        return invisibleTile;
    }

    public AnimatedTiledMapTile getVisibleTile() {
        return visibleTile;
    }

    public void setCell(TiledMapTileLayer.Cell cell) {
        this.cell = cell;
    }
=======
        pickedUp = false;

        //Setting the tile
        Iterator<TiledMapTile> tiles = Play.getMap().getTileSets().getTileSet("tileSet").iterator();
        while(tiles.hasNext()) {
            TiledMapTile tile = tiles.next();
            if (tile.getId() == 30) {
            coinTile = (AnimatedTiledMapTile)tile;
            }
            else if(tile.getId() == 672)
                emptyTile = (AnimatedTiledMapTile)tile;
        }
    }

    @Override
    public AnimatedTiledMapTile getTile() {
        return coinTile;
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public void setId(int i) {

    }

    @Override
    public BlendMode getBlendMode() {
        return null;
    }

    @Override
    public void setBlendMode(BlendMode blendMode) {

    }

    @Override
    public TextureRegion getTextureRegion() {
        return null;
    }

    @Override
    public void setTextureRegion(TextureRegion textureRegion) {

    }

    @Override
    public float getOffsetX() {
        return 0;
    }

    @Override
    public void setOffsetX(float v) {

    }

    @Override
    public float getOffsetY() {
        return 0;
    }

    @Override
    public void setOffsetY(float v) {

    }

    @Override
    public MapProperties getProperties() {
        return null;
    }

>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
}
