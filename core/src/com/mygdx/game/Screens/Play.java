package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.Entities.CoinBox;
import com.mygdx.game.Entities.Player;

import java.io.File;
import java.util.ArrayList;

import static com.mygdx.game.Entities.Player.getCollisionLayer;

public class Play implements Screen {


    private static TiledMap map;
    private OrthogonalTiledMapRenderer mapRenderer;
    private OrthographicCamera camera;
    private Player player;
    private Texture livesTexture;
    private static ArrayList<CoinBox> coinBoxes;
<<<<<<< HEAD
    private BitmapFont gameOverFont;
    private BitmapFont gameInfoFont;
=======
    //Fonts
    private FreeTypeFontGenerator fontGenerator;
    private FreeTypeFontGenerator.FreeTypeFontParameter gameOverFontParameter;
    private BitmapFont gameOverFont;
    private FreeTypeFontGenerator.FreeTypeFontParameter timeFontParameter;
    private BitmapFont timeFont;
    private BitmapFont pointsFont;
    private FreeTypeFontGenerator.FreeTypeFontParameter pointsFontParameter;

>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
    private float START_TIME = System.nanoTime();
    private float END_TIME;

    private int currentMap;
<<<<<<< HEAD
    private static int points;

=======

    private ShapeRenderer shapeRenderer;
    private ArrayList<MapObject> mapObjects;
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c

    private static int points = 0;

    @Override
    public void show() {
        map = new TmxMapLoader().load("Maps/lvl1.tmx");
        currentMap = 1;
        mapRenderer = new OrthogonalTiledMapRenderer(map);

<<<<<<< HEAD
=======
        mapObjects = new ArrayList<>();
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
        livesTexture = new Texture("Textures/heart.png");
        camera = new OrthographicCamera();
        player = new Player(new Sprite(new Texture("Textures/Actors/Jolly.png")), (TiledMapTileLayer)(map.getLayers().get("collisionLayer")));
        player.setPosition(4 * getCollisionLayer().getTileWidth(), 4 * getCollisionLayer().getTileHeight());
        camera.position.set((player.getX() + player.getWidth())/2, (player.getY() + player.getHeight())/2, 0);
        camera.update();

        Gdx.input.setInputProcessor(player);

        //fonts
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("Fonts/PixelEmulator.otf"));

        //game over font
        FreeTypeFontGenerator.FreeTypeFontParameter gameOverFontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        gameOverFontParameter.size = 50;
        gameOverFontParameter.color = Color.WHITE;
        gameOverFont = fontGenerator.generateFont(gameOverFontParameter);
<<<<<<< HEAD
        //game info font
        FreeTypeFontGenerator.FreeTypeFontParameter gameInfoFontParameter = gameOverFontParameter;
        gameInfoFontParameter.size = 20;
        gameInfoFont = fontGenerator.generateFont(gameInfoFontParameter);
=======

        //Time
        timeFontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        timeFontParameter = gameOverFontParameter;
        timeFontParameter.size = 20;
        timeFont = fontGenerator.generateFont(timeFontParameter);
        //Points
        pointsFontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        pointsFontParameter = timeFontParameter;
        pointsFont = fontGenerator.generateFont(pointsFontParameter);

>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c

        //Scanning for coin boxes
        coinBoxes = new ArrayList<>();
        TiledMapTileLayer.Cell tempCell;
        for(int i = 0; i < getCollisionLayer().getWidth(); i++) {
            for(int j = 0; j < getCollisionLayer().getHeight(); j++){
                tempCell = getCollisionLayer().getCell(i, 29 - j);
                if(tempCell != null && tempCell.getTile().getProperties().containsKey("coinBox")){
                    coinBoxes.add(new CoinBox(i, 29 - j));
                }
            }
        }
        //Scanning map objects
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        mapRenderer.setView(camera);
        camera.position.set(player.getX(), player.getY(), 0);
        limitCamera();
        camera.update();
        mapRenderer.render();

        mapRenderer.getBatch().begin();
        player.draw(mapRenderer.getBatch()); //drawing the player
        for(int i = 1; i <= player.getLives(); i++) { // drawing player lives
            mapRenderer.getBatch().draw(livesTexture, i * 35 + camera.position.x - 320, Gdx.graphics.getHeight() - 50 , 30, 30);
        }
        //Viewing coin boxes
        for(CoinBox b: coinBoxes) {
<<<<<<< HEAD
                getCollisionLayer().getCell(b.getX(), b.getY()).setTile(b.getCurrentTile());
                getCollisionLayer().getCell(b.getLoot().getX(), b.getLoot().getY()).setTile(b.getLoot().getCurrentTile());
=======
                player.getCollisionLayer()
                        .getCell(b.getX(), b.getY())
                        .setTile(b.getCurrentTile());

                if(b.isOpened())
                    player.getCollisionLayer()
                            .getCell(b.getLoot().getX(), b.getLoot().getY())
                            .setTile(b.getLoot().getTile());
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
        }
        gameOver();
        pointsFont.draw(mapRenderer.getBatch(), "Points: " + points, camera.position.x + 50 , 465);

        mapRenderer.getBatch().end();
    }

    public void limitCamera(){
        camera.position.x = MathUtils.clamp(camera.position.x, 320, getCollisionLayer().getTileWidth() * getCollisionLayer().getWidth() - 320);
        camera.position.y = MathUtils.clamp(camera.position.y, 240, getCollisionLayer().getTileHeight() * getCollisionLayer().getHeight() - 240);
    }

    public void restart() {
        player.reset();
        player.setPosition(4 * getCollisionLayer().getTileWidth(), 4 * getCollisionLayer().getTileHeight());
        for(CoinBox b: coinBoxes) {
        b.closeCoinBox();
        getCollisionLayer().getCell(b.getX(), b.getY()).setTile(b.getCurrentTile());
        }
        START_TIME = System.nanoTime();
    }

    public ArrayList<String> getMaps() {
        File contents = new File("Maps");
        ArrayList<String> maps = new ArrayList<>();
        for(File f: contents.listFiles())
            if(f.getName().contains(String.valueOf("lvl")))
                maps.add(f.getName());
            return maps;
    }

    public void nextMap() {

        if(currentMap >= getMaps().size())
            currentMap = 0;

        map = new TmxMapLoader().load("Maps/" + getMaps().get(currentMap));
        mapRenderer = new OrthogonalTiledMapRenderer(map);
        player.setCollisionLayer((TiledMapTileLayer)map.getLayers().get("collisionLayer"));
        mapRenderer.getBatch().begin();
        //Scanning for coin boxes
        coinBoxes = new ArrayList<>();
        TiledMapTileLayer.Cell tempCell;
        for(int i = 0; i < getCollisionLayer().getWidth(); i++) {
            for(int j = 0; j < getCollisionLayer().getHeight(); j++){
                tempCell = getCollisionLayer().getCell(i, 29 - j);
                if(tempCell != null && tempCell.getTile().getProperties().containsKey("coinBox")){
                    coinBoxes.add(new CoinBox(i, 29 - j));
                }
            }
        }
        currentMap++;
    }

<<<<<<< HEAD
=======
    public ArrayList<MapObject> getMapObjects(){

        ArrayList<MapObject> result = new ArrayList<>();
        if(map.getLayers().get("objectLayer") != null){
            for(MapObject o: map.getLayers().get("objectLayer").getObjects()){
                result.add(o);
            }
        }
        return result;
    }

    public static int getPoints() {
        return points;
    }

    public static void setPoints(int points) {
        points = points;
    }

    public static void addPoints(int points) {
        Play.points += points;
    }

    public void renderMapObjects(){
        for(MapObject o: getMapObjects()){
            if(o instanceof RectangleMapObject){
                Rectangle rec = ((RectangleMapObject) o).getRectangle();
                shapeRenderer = new ShapeRenderer();
                shapeRenderer.setProjectionMatrix(camera.combined);
                shapeRenderer.setAutoShapeType(true);
                shapeRenderer.begin();
                shapeRenderer.rect(rec.x, rec.y, rec.width, rec.height);
                System.out.println(rec);
                shapeRenderer.end();
            }
            else if(o instanceof EllipseMapObject) {
                /*Ellipse ellipse = ((EllipseMapObject) o).getEllipse();
                shapeRenderer = new ShapeRenderer();
                shapeRenderer.setProjectionMatrix(camera.combined);
                shapeRenderer.setColor(Color.BLACK);
                //shapeRenderer.begin(ShapeRenderer.ShapeType.);
                shapeRenderer.ellipse(ellipse.x, ellipse.y, ellipse.width, ellipse.height);
                System.out.println(ellipse);
                shapeRenderer.end();*/
            }
        }
    }

>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
    @Override
    public void resize(int i, int i1) {
        camera.viewportWidth = i;
        camera.viewportHeight = i1;
        camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    public static ArrayList<CoinBox> getCoinBoxes() {
        return coinBoxes;
    }

    public static TiledMap getMap() {
        return map;
    }

    public void gameOver() {
        if(player.getLives() <= 0){
            gameOverFont.draw(mapRenderer.getBatch(), "GAME OVER", Gdx.graphics.getWidth() - 800 + camera.position.x , Gdx.graphics.getHeight() - 450 + camera.position.y);
            if(END_TIME == 0.0f) //stop time at death
                END_TIME = System.nanoTime();
            timeFont.draw(mapRenderer.getBatch(), "Time: " + (Math.ceil((END_TIME - START_TIME)/ Math.pow(10, 8))) / 10, 500 + camera.position.x , 465);
            if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){ // restarting the game upon death
                END_TIME = 0.0f;
                restart();
                nextMap();
            }
        }
        else
            timeFont.draw(mapRenderer.getBatch(), "Time: " + (Math.ceil((System.nanoTime() - START_TIME)/ Math.pow(10, 8))) / 10, camera.position.x + 190 , 465); // view time
    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}


//TODO organize the code
//TODO remove pointless spaces
//TODO organize setters and getters
//TODO add screen borders
//TODO make the map move, not the player
