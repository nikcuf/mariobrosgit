<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="tileSet" tilewidth="16" tileheight="16" tilecount="924" columns="33">
 <image source="modelsSet.png" width="528" height="448"/>
 <terraintypes>
  <terrain name="Nowy Teren" tile="0">
   <properties>
    <property name="blocked" value=""/>
   </properties>
  </terrain>
 </terraintypes>
 <tile id="0">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="blocked" type="bool" value="false"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="openedCoinBox" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="28">
  <animation>
   <frame tileid="24" duration="300"/>
   <frame tileid="25" duration="300"/>
   <frame tileid="26" duration="300"/>
  </animation>
 </tile>
 <tile id="29" type="Coin">
  <properties>
   <property name="pickup" type="bool" value="false"/>
  </properties>
  <animation>
   <frame tileid="57" duration="300"/>
   <frame tileid="58" duration="300"/>
   <frame tileid="59" duration="300"/>
  </animation>
 </tile>
 <tile id="33">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="35">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="36">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="37">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="38">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="67">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="68">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="69">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="70">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="71">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="72">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="76">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="78">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="99">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="100">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="102">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="103">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="109">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="110">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="111">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="115">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="116">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="117">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="118">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="119">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="132">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="133">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="134">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="135">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="136">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="137">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="138">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="139">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="140">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="141">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="142">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="143">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="146">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="147">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="148">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="149">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="150">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="151">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="152">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="153">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="154">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="155">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="156">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="165">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="166">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="167">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="168">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="169">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="170">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="171">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="172">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="173">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="174">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="175">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="176">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="177">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="178">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="179">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="180">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="181">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="182">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="183">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="184">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="185">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="186">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="187">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="188">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="198">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="199">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="200">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="201">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="202">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="203">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="204">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="205">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="206">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="207">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="208">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="209">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="210">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="211">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="212">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="213">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="214">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="215">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="216">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="217">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="218">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="219">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="220">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="221">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="222">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="231">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="232">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="233">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="234">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="235">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="236">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="237">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="238">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="239">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="240">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="241">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="242">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="243">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="244">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="245">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="246">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="247">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="248">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="249">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="250">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="251">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="252">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="253">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="254">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="264">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="265">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="266">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="267">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="268">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="269">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="270">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="271">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="272">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="273">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="274">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="275">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="276">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="277">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="278">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="279">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="280">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="281">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="282">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="283">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="284">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="285">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="286">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="287">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="290">
  <properties>
   <property name="invisibleTile" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="290" duration="300"/>
   <frame tileid="291" duration="300"/>
  </animation>
 </tile>
 <tile id="297">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="298">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="299">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="300">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="301">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="302">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="303">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="304">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="305">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="306">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="307">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="308">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="309">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="310">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="311">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="312">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="313">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="314">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="315">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="316">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="317">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="318">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="319">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="320">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="323">
  <properties>
<<<<<<< HEAD
   <property name="coin" type="bool" value="true"/>
   <property name="pickup" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="57" duration="300"/>
   <frame tileid="58" duration="300"/>
   <frame tileid="59" duration="300"/>
  </animation>
 </tile>
 <tile id="324">
  <animation>
=======
   <property name="coinBox" type="bool" value="true"/>
  </properties>
  <animation>
>>>>>>> dc307f2ca8174b07820c350f3251aa5df509f66c
   <frame tileid="24" duration="300"/>
   <frame tileid="25" duration="300"/>
   <frame tileid="26" duration="300"/>
  </animation>
 </tile>
 <tile id="330">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="331">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="332">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="333">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="334">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="335">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="336">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="337">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="338">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="339">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="340">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="341">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="342">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="343">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="344">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="345">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="346">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="347">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="348">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="349">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="350">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="351">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="352">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="353">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="363">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="364">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="365">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="366">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="367">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="368">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="369">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="370">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="371">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="372">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="373">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="374">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="375">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="376">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="377">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="378">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="379">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="380">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="381">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="382">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="383">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="384">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="385">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="386">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="396">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="397">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="398">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="399">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="400">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="401">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="402">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="403">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="404">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="405">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="406">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="407">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="408">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="409">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="410">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="411">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="412">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="413">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="414">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="415">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="416">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="417">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="418">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="419">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="429">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="430">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="431">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="432">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="433">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="434">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="435">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="436">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="437">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="438">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="439">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="440">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="441">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="442">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="443">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="444">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="445">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="446">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="447">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="448">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="449">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="450">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="451">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="452">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="462">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="463">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="464">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="465">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="466">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="467">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="468">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="469">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="470">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="471">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="472">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="473">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="474">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="475">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="476">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="477">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="478">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="479">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="480">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="481">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="482">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="483">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="484">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="485">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="495">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="496">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="497">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="498">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="499">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="500">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="501">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="502">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="503">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="504">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="505">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="506">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="507">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="508">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="509">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="510">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="511">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="512">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="513">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="514">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="515">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="516">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="517">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="518">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="528">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="529">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="530">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="531">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="532">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="533">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="534">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="535">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="536">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="537">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="538">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="539">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="540">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="541">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="542">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="543">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="544">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="545">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="546">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="547">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="548">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="549">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="550">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="551">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="561">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="562">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="563">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="564">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="565">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="566">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="567">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="568">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="569">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="570">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="571">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="572">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="573">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="574">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="575">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="576">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="577">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="578">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="579">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="580">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="581">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="582">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="583">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="584">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="594">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="595">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="596">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="597">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="598">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="599">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="600">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="601">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="602">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="603">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="604">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="605">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="606">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="607">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="608">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="609">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="610">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="611">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="612">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="613">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="614">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="615">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="616">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="617">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="627">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="628">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="629">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="630">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="631">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="632">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="633">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="634">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="635">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="636">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="637">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="638">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="639">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="640">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="641">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="642">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="643">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="644">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="645">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="646">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="647">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="648">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="649">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="650">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="660">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="661">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="662">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="663">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="664">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="665">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="666">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="667">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="668">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="669">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="670">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="671">
  <properties>
   <property name="emptyTile" type="bool" value="true"/>
  </properties>
  <animation>
   <frame tileid="671" duration="300"/>
   <frame tileid="672" duration="300"/>
  </animation>
 </tile>
 <tile id="672">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="673">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="674">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="675">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="676">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="677">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="678">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="679">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="680">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="681">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="682">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="683">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="693">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="694">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="695">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="696">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="697">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="698">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="699">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="700">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="701">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="702">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="703">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="704">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="705">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="706">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="707">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="708">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="709">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="710">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="711">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="712">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="713">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="714">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="715">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="716">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="726">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="727">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="728">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="729">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="730">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="731">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="732">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="733">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="734">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="735">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="736">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="737">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="738">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="739">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="740">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="741">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="742">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="743">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="744">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="745">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="746">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="747">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="748">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="749">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="759">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="760">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="761">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="762">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="763">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="764">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="765">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="766">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="767">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="768">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="769">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="770">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="771">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="772">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="773">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="774">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="775">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="776">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="777">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="778">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="779">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="780">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="781">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="782">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="792">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="793">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="794">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="795">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="796">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="797">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="798">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="799">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="800">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="801">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="802">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="803">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="804">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="805">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="806">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="807">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="808">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="809">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="810">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="811">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="812">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="813">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="814">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="815">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="825">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="826">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="827">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="828">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="829">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="830">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="831">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="832">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="833">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="834">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="835">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="836">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="837">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="838">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="839">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="840">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="841">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="842">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="843">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="844">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="845">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="846">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="847">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="848">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="858">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="859">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="860">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="861">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="862">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="863">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="864">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="865">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="866">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="867">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="868">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="869">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="870">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="871">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="872">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="873">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="874">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="875">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="876">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="877">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="878">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="879">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="880">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="881">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="891">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="892">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="893">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="894">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="895">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="896">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="897">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="898">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="899">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="900">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="901">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="902">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="903">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="904">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="905">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="906">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="907">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="908">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="909">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="910">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="911">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="912">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="913">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="914">
  <properties>
   <property name="blocked" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
